package com.unlimited.demo.rest.spring.service;

/**
 * Commercial not found exception.
 * Use this to indicate that a commercial (or a collection of commercials) wasn't found.
 *
 * @author Iulian Dumitru
 */
public class CommercialNotFoundException extends EntityNotFoundException {

	public CommercialNotFoundException(String message) {
		super(message);
	}

}
