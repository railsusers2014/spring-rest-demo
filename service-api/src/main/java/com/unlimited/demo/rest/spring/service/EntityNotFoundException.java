package com.unlimited.demo.rest.spring.service;

import com.unlimited.demo.rest.spring.commons.exception.BusinessException;

/**
 * Exception to indicate that a platform entity was not found.
 *
 * @author Iulian Dumitru
 */
public class EntityNotFoundException extends BusinessException {

    public EntityNotFoundException() {
    }

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
