package com.unlimited.demo.rest.spring.repository;

import com.unlimited.demo.rest.domain.Commercial;
import com.unlimited.demo.rest.spring.service.PageRequest;

import java.util.Date;
import java.util.List;

/**
 * @author Iulian Dumitru
 */
public interface CommercialRepository {

    /**
     * Get a list of commercials price based on specific parameters.
     *
     * @param channel       channel
     * @param from          beginning of the time interval
     * @param to            end of the time interval
     * @param duration      duration for the commercial (in seconds)
     * @param occurrencesNr number of occurrences of the commercial
     */
    List<Commercial> getCommercials(String channel, Date from, Date to, long duration, long occurrencesNr, PageRequest pageRequest);

    /**
     * Return existing channels
     *
     * @return existing channels
     */
    List<String> getChannels();

}
