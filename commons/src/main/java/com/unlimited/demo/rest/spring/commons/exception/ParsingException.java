package com.unlimited.demo.rest.spring.commons.exception;

/**
 * Parsing exception.
 *
 * @author Iulian Dumitru
 */
public class ParsingException extends SystemException {

    public ParsingException() {
    }

    public ParsingException(String message) {
        super(message);
    }

}
