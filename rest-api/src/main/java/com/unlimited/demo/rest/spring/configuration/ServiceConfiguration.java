package com.unlimited.demo.rest.spring.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Spring services configuration.
 * (exclude @EnableWebMvc configurations so that unit tests will run )
 *
 * @author Iulian Dumitru
 */
@ComponentScan(basePackages = "com.unlimited", excludeFilters = {@ComponentScan.Filter(EnableWebMvc.class)})
@PropertySource("classpath:/rest.properties")
@Configuration
public class ServiceConfiguration {

    @Bean
    public LocalValidatorFactoryBean localValidatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer props() {
        return new PropertySourcesPlaceholderConfigurer();
    }


}
