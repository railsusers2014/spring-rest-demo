package com.unlimited.demo.rest.domain;

import com.google.common.base.Objects;

import java.math.BigDecimal;
import java.util.List;

/**
 * Commercial domain type.
 *
 * @author Iulian Dumitru
 */
public class Commercial {

    /**
     * The channels on which the commercial appears
     */
    private String channel;

    /**
     * Duration for the commercial (in seconds)
     */
    private int duration;

    /**
     * The name of the commercial
     */
    private String name;

    /**
     * The description of the commercial
     */
    private String description;

    /**
     * The price of the commercial
     */
    private BigDecimal price;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(channel, duration, price);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Commercial) {
            final Commercial other = (Commercial) obj;
            return Objects.equal(channel, other.channel) &&
                    Objects.equal(duration, other.duration) &&
                    Objects.equal(name, other.name) &&
                    Objects.equal(description, other.description) &&
                    price.compareTo(other.price) == 0;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("channel", channel)
                .add("price", price)
                .add("name", name)
                .add("description", description)
                .toString();
    }

}
